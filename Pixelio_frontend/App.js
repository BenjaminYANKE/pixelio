 import 'react-native-gesture-handler';
 import { StyleSheet } from 'react-native';
 import { NavigationContainer } from '@react-navigation/native';
 import { createStackNavigator } from '@react-navigation/stack';
 import React from 'react';
 import HomePage from './Components/HomePage';
 import AlbumPage from './Components/AlbumPage';
 import AlbumContentPage from './Components/AlbumContentPage';
 import ImageDetailPage from './Components/ImageDetailPage';

 const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomePage} style={styles.title} />
        <Stack.Screen name="Albums" component={AlbumPage}  />
        <Stack.Screen name="AlbumContent" component={AlbumContentPage} />
        <Stack.Screen name="ImageDetail" component={ImageDetailPage} />
        {/* Define other screens as needed */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  title: {
    flex: 1,
    backgroundColor: 'cyan',
  }})

export default App;
