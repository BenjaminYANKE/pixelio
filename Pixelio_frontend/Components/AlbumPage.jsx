import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, Alert, Modal, TextInput, FlatList } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';
import NavBar from './NavBar';

const AlbumPage = () => {
  const navigation = useNavigation();
  const [albums, setAlbums] = useState([]);
  const [currentPage, setCurrentPage] = useState('Albums');
  const [filteredAlbums, setFilteredAlbums] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [newAlbumName, setNewAlbumName] = useState('');
  const [creatingAlbum, setCreatingAlbum] = useState(false);
  const [selectedImages, setSelectedImages] = useState([]);
  const [albumName, setAlbumName] = useState("");
  const APIURL = 'http://10.1.5.161:3000';

  //10.1.7.122
  //10.1.5.161
  useEffect(() => {
    fetchAlbums();
  }, []);

  const fetchAlbums = async () => {
    try {
      const response = await fetch(`${APIURL}/albums`);
      const data = await response.json();
      setAlbums(data);
    } catch (error) {
      console.error('Failed to fetch albums:', error);
    }
  };
  useEffect(() => {
    if (searchQuery) {
      const searchLower = searchQuery.toLowerCase();
      const newFilteredAlbums = albums.filter(album =>
        album.name.toLowerCase().includes(searchLower)
      );
      setFilteredAlbums(newFilteredAlbums);
    } else {
      setFilteredAlbums(albums); // No search query, shows all albums
    }
  }, [searchQuery, albums]);

  useEffect(() => {
    if (selectedImages.length > 0) {
      // Show modal or input for album name
      setModalVisible(true);
    }
  }, [selectedImages]);

  // Handle final album creation
  const submitNewAlbum = async () => {
    const albumData = {
      albumName,
      images: selectedImages,
    };
    // Submit to backend
    await fetch(`${APIURL}/albums`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(albumData),
    });
    // Reset states and fetch albums again
    setCreatingAlbum(false);
    setSelectedImages([]);
    setAlbumName("");
    fetchAlbums();
  };

  const handleSelectAlbum = (album) => {
    navigation.navigate('AlbumContent', { 
      albumId: album._id, 
      albumName: album.albumName,
      //updateAlbumName: (newName) => updateAlbumName(album._id, newName) // Passing a callback function
    });
  };
  
  // // Function to update the album's name in the current state
  // const updateAlbumName = (albumId, newName) => {
  //   setAlbums(albums.map(album => album._id === albumId ? { ...album, albumName: newName } : album));
  // };
  

  const handleDeleteAlbum = async (albumId) => {
    Alert.alert('Confirm', 'Are you sure you want to delete this album?', [
      { text: 'Cancel', style: 'cancel' },
      {
        text: 'OK', onPress: async () => {
          await fetch(`${APIURL}/albums/${albumId}`, { method: 'DELETE' });
          fetchAlbums();
        }
      },
    ]);
  };

  const renderItem = ({ item: album }) => (
    <TouchableOpacity style={styles.albumCard} onPress={() => handleSelectAlbum(album)}>
      {album.previewImageUrl ? (
        <Image
          source={{ uri: `${APIURL}/uploads/${album.previewImageUrl}` }}
          style={styles.albumImage}
        />
      ) : (
        <View style={styles.albumImagePlaceholder}>
          <Text>No Image</Text>
        </View>
      )}
      <View style={styles.modification}>
        <Text style={styles.albumTitle}>{album.albumName}</Text>
        <TouchableOpacity style={styles.deleteButton} onPress={() => handleDeleteAlbum(album._id)}>
          <Icon name="trash" size={20} color="black" />
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );  
  return (
    <View style={styles.container}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.modalView}>
          <TextInput
            placeholder="Album Name"
            value={newAlbumName}
            onChangeText={setNewAlbumName}
            style={styles.input}
          />
          {/* <TouchableOpacity style={styles.button} onPress={submitNewAlbum}>
            <Text style={styles.buttonText}>Add Album</Text>
          </TouchableOpacity> */}
        </View>
      </Modal>

      <View style={styles.header}>
      <Text style={styles.title}>Pixelio</Text>
      </View>
      <FlatList
        data={albums}
        renderItem={renderItem}
        keyExtractor={(item) => item._id}
        numColumns={2}
        contentContainerStyle={styles.albumGrid}
      />
      {/* // on rendering NavBar,i'm passing searchQuery and setSearchQuery, currentPage as props */}
      <NavBar
        searchQuery={searchQuery}
        setSearchQuery={setSearchQuery}
        currentPage={'Albums'}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // padding: 20,
    textAlign: 'center',
    backgroundColor: 'lightblue',
    height:60
  },
  albumGrid: {
    padding: 10,
  },
  albumCard: {
    flex: 1,
    margin: 5,
    borderRadius: 10,
    overflow: 'hidden',
    elevation: 3,
  },
  albumImage: {
    width: '100%',
    height: 150,
  },
  albumTitle: {
  },
  deleteButton: {
   paddingLeft:40 
  },
  modalView: {
  },
  input: {
  },
  button: {
  },
  buttonText: {
  }, albumList: {
    paddingHorizontal: 10,
  },
  modification:{
    flexDirection: 'row', 
    alignItems: 'center',
    
    
  },
  albumCard: {
    flex: 1,
    margin: 5,
    borderRadius: 8,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 40,
    color: '#ffffff',
    fontFamily:'Palatino',
    // paddingLeft:130
    textAlign: 'center'

  },
  albumImage: {
    width: '100%',
    height: 150,
    borderRadius: 8,
  },
  albumTitle: {
    marginTop: 8,
    fontSize: 16,
    fontWeight: 'bold',
  },
});
export default AlbumPage;