import React, { useState, useEffect } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Image, View, Text,TextInput, StyleSheet, TouchableOpacity, FlatList, ScrollView } from 'react-native';
import NavBar from './NavBar';
import { useRoute , useNavigation} from '@react-navigation/native';

const AlbumContentPage = () => {
  const navigation = useNavigation();
  const [images, setImages] = useState([]);
  const route = useRoute();
  // Destructuring the albumName and albumId from route.params
  const { albumName, albumId } = route.params;
  const [editMode, setEditMode] = useState(false);
  const [newAlbumName, setNewAlbumName] = useState('');

  const APIURL = 'http://10.1.5.161:3000'

  const [albumDetails, setAlbumDetails] = useState({});

  useEffect(() => {
    fetchAlbumImages(albumId); // passing albumId
  }, [albumId]); // Add albumId as a dependency
  

  const fetchAlbumImages = async () => {
    try {
      console.log("Album ID:", albumId);
      // Ensure you dynamically insert the album._id into the URL
      const url = `${APIURL}/albums/${albumId}/images`;
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const data = await response.json();
      setImages(data); // Assuming the API returns an array of images directly
    } catch (error) {
      console.error('Failed to fetch images for the album:', error);
    }
  };
  
  // const removeAlbumFromState = (deletedAlbumId) => {
    
  // };

  // When navigating to ImageDetail
  const handleSelectImage = (image) => {
    navigation.navigate('ImageDetail', { image });
  };
  const editAlbumName = async () => {
    if (!newAlbumName.trim()) {
      alert('Album name cannot be empty.');
      return;
    }
    try {
      const response = await fetch(`${APIURL}/albums/${albumId}`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ albumName: newAlbumName }),
      });
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const updatedAlbum = await response.json();
      // Update the album name in the UI, and close the edit mode
      setAlbumDetails({ ...albumDetails, albumName: updatedAlbum.albumName });
      setEditMode(false);
    } catch (error) {
      console.error('Failed to update album name:', error);
    }
  };
  

  return (
    <View style={styles.container}>
  <View style={styles.header}>
    {editMode ? (
      <>
        <TextInput
          style={styles.input}
          onChangeText={setNewAlbumName}
          value={newAlbumName}
          placeholder="Enter new album name"
        />
        <TouchableOpacity onPress={editAlbumName}>
          <Icon name="check" size={20} color="#fff" />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setEditMode(false)}>
          <Icon name="times" size={20} color="#fff" />
        </TouchableOpacity>
      </>
    ) : (
      <>
        <Text style={styles.title}>{albumName}</Text>
        <View style={styles.actions}>
          <TouchableOpacity onPress={() => setEditMode(true)}>
            <Icon name="edit" size={20} color="#fff" />
          </TouchableOpacity>
          {/* Uncomment the next block if you want to add delete functionality */}
          {/* <TouchableOpacity onPress={removeAlbumFromState} style={styles.deleteButton}>
            <Icon name="trash" size={20} color='#fff' />
          </TouchableOpacity> */}
        </View>
      </>
    )}
  </View>
  <FlatList
  data={images}
  renderItem={({ item }) => {
    const image = item; // 'item' is the image's object
    return (
      <View style={styles.imageGrid}>
        <TouchableOpacity key={image._id} style={styles.imageContainer} onPress={() => handleSelectImage(image)}>
          <Image source={{ uri: `${APIURL}/uploads/${image.imageUrl}` }} style={styles.image} />
        </TouchableOpacity>
      </View>
    );
  }}
  keyExtractor={(item) => item._id.toString()} // Using '_id' as the key
  />
  <NavBar />
</View>
  );
        };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff', // light background for the page
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
    backgroundColor: 'lightblue',
    height:60 // color to match your design
  },
  title: {
    flex: 1,
    fontSize: 30,
    color: '#ffffff',
    fontFamily:'Palatino',
  },
  actions: {
    flexDirection: 'row',
    width: 80, // space for both buttons
    justifyContent: 'space-between',
  },
 
  dateGroup: {
    marginVertical: 8,
  },
  dateText: {
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'left',
  },
  imageGrid: {
    flexDirection: 'row',
    // flexWrap: 'wrap',
    // justifyContent: 'space-between',
    padding: 4,
  },
  imageContainer: {
    width: '32%', // Ajustez la largeur en fonction de vos besoins pour afficher trois images par ligne
    aspectRatio: 1, // Garantit que les images sont carrées
    padding: 4,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 10, // Ajustez le rayon de bordure
  },
  input: {
    // Add your input styles here
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },  
});

export default AlbumContentPage;

