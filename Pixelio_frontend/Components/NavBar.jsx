import React, { useState, useRef } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import { View, TouchableOpacity, Animated, TextInput, Text, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const NavBar = ({ searchQuery, setSearchQuery, currentPage }) => {
  const navigation = useNavigation();
  const [isSearchVisible, setIsSearchVisible] = useState(false);
  const searchInputWidth = useRef(new Animated.Value(0)).current; // Initial width set to 0

  const slideOutDuration = 300; // milliseconds
  const slideInDuration = 300; // milliseconds

  const slideOutSearchInput = () => {
    Animated.timing(searchInputWidth, {
      toValue: 0,
      duration: slideOutDuration,
      useNativeDriver: false, // width does not support native driver
    }).start(() => setIsSearchVisible(false));
  };

  const slideInSearchInput = () => {
    setIsSearchVisible(true);
    Animated.timing(searchInputWidth, {
      toValue: 200, // or the width of your search input field
      duration: slideInDuration,
      useNativeDriver: false,
    }).start();
  };

  const toggleSearch = () => {
    if (isSearchVisible) {
      slideOutSearchInput();
    } else {
      slideInSearchInput();
    }
  };
  // This function will be called when the search input changes
  const handleSearch = text => {
    setSearchQuery(text);
  };
  return (
    <View style={styles.navBar}>
      <TouchableOpacity onPress={() => navigation.navigate('Home')} style={styles.navItem}><Text style={styles.navText}>Photos</Text></TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('Albums')} style={styles.navItem}><Text style={styles.navText}>Albums</Text></TouchableOpacity>
      <TouchableOpacity onPress={() => { toggleSearch() }} style={styles.navItems}><Icon name="search" size={25} color="#ffffff" />{/* Adjust size and color as needed */}</TouchableOpacity>
      {isSearchVisible && (
        <Animated.View style={{ width: searchInputWidth }}>
          <TextInput
            placeholder={`Search ${currentPage === 'Home' ? 'images' : 'Albums'}...`}
            value={searchQuery}
            onChangeText={handleSearch}
            style={styles.searchInput}
            autoFocus={true}
          />
        </Animated.View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  navBar: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: 'lightblue',
    paddingBottom: 10,
    paddingTop: 20,
    borderTopWidth: 1,
    borderTopColor: '#ffffff',
  },
  navItem: {
    alignItems: 'center',
    justifyContent: 'center',
    // borderBottomLeftRadius:'100%',
    borderTopRightRadius: 50,
    borderBottomLeftRadius: 50,
    borderTopLeftRadius: 50,
    borderBottomRightRadius: 50,
    borderRadius: 10,
    borderWidth: 5,
    borderColor: 'white',
    padding: 10
  },
  navItems: {
    alignItems: 'center',
    justifyContent: 'center',
    // borderBottomLeftRadius:'100%',


    // borderWidth: 10, 
    // borderColor: 'rgba(128, 128, 128, 0.5)',
    padding: 2
  },
  navText: {
    color: '#ffffff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  searchInput: {
    width: '100%',
  },
});
export default NavBar;
