import React, { useState, useEffect } from 'react';
import * as ImagePicker from 'expo-image-picker';
import Icon from 'react-native-vector-icons/FontAwesome';
import { View, Text, TouchableOpacity, StyleSheet, Image, FlatList } from 'react-native';
import { format, parseISO } from 'date-fns';
import { useNavigation } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import NavBar from './NavBar';
const HomePage = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [filteredImages, setFilteredImages] = useState([]);
  const [currentPage, setCurrentPage] = useState('Home');
  const [images, setImages] = useState([]);
  const navigation = useNavigation();
  const APIURL = 'http://10.1.5.161:3000'
  //10.1.7.122

  // const groupImagesByDate = (images) => {
  //   const groups = {};
  //   images.forEach((img) => {
  //     const date = parseISO(img.imageCreationTimestamp);
  //     const dateStr = format(date, 'PPP');
  //     if (!groups[dateStr]) {
  //       groups[dateStr] = [];
  //     }
  //     groups[dateStr].push(img);
  //   });
  //   return groups;
  // };
  const groupImagesByDate = (images) => {
    const groups = {};
    images.forEach((img) => {
      // Ensure the date is in ISO format to ease sorting
      const date = parseISO(img.imageCreationTimestamp);
      const dateISOStr = format(date, 'yyyy-MM-dd'); // sortable date format

      if (!groups[dateISOStr]) {
        groups[dateISOStr] = [];
      }
      groups[dateISOStr].push(img);
    });

    // Sorts the dates in descending order
    const sortedDates = Object.keys(groups).sort((a, b) => new Date(b) - new Date(a));

    // Maps the sorted dates back  for display
    const sortedGroups = {};
    sortedDates.forEach((dateISOStr) => {
      const date = parseISO(dateISOStr);
      const dateStr = format(date, 'PPP'); // Convert back to human-readable format for display
      sortedGroups[dateStr] = groups[dateISOStr];
    });

    return sortedGroups;
  };

  //This helps for better state management instead of useEffect
  useFocusEffect(
    React.useCallback(() => {
      const fetchImages = async () => {
        try {
          const response = await fetch(`${APIURL}/images`);
          const data = await response.json();
          const groupedImages = groupImagesByDate(data);
          setImages(groupedImages);
          console.log('fetch successful');
        } catch (error) {
          console.error('Failed to fetch images:', error);
        }
      };

      fetchImages();
    }, [])
  );
  useEffect(() => {
    // Assuming images are already fetched and grouped by date
    if (searchQuery) {
      const searchLower = searchQuery.toLowerCase();
      const newFilteredImages = images.filter(group =>
        group.date.toLowerCase().includes(searchLower)
      );
      setFilteredImages(newFilteredImages);
    } else {
      setFilteredImages(images); // No search query, show all images
    }
  }, [searchQuery, images]);

  const removeImageFromState = (deletedImageId) => {
    const updatedImages = Object.entries(images).reduce((acc, [date, imgs]) => {
      // Filtering out the deleted image
      const filteredImages = imgs.filter(img => img._id !== deletedImageId);
      if (filteredImages.length > 0) {
        acc[date] = filteredImages;
      }
      return acc;
    }, {});
    setImages(updatedImages)
  };

  // When navigating to ImageDetail
  const handleSelectImage = (image) => {
    navigation.navigate('ImageDetail', { image });
  };
  const handleAddImage = async () => {
    // Requesting permission
    const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (status !== 'granted') {
      alert('Sorry, we need camera roll permissions to make this work!');
      return;
    }

    // Launching the image library for multiple images
    const response = await ImagePicker.launchImageLibraryAsync({
      mediaType: ImagePicker.MediaTypeOptions.Images,
      quality: 1,
      allowsMultipleSelection: true, // Allows multiple selection
      selectionLimit: 10, // Maximum number of images to be selected
    });

    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else if (!response.cancelled) {
      // assets and forEach handles multiple images upload
      response.assets.forEach(image => uploadImage(image.uri));
    }
  };

  const uploadImage = async (uri) => {
    const formData = new FormData();
    formData.append('images', {
      uri: uri,
      type: 'image/jpeg',
      name: 'upload.jpg',
    });

    try {
      const response = await fetch(`${APIURL}/images`, {
        method: 'POST',
        body: formData,
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      const responseData = await response.json();
      console.log(responseData);
    } catch (error) {
      console.error('Upload Error:', error);
    }
  };
  //const imageUrl = 'http://192.168.1.16:3000/uploads\\images-1708803633369.jpg' just trying
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.title}>Pixelio</Text>
        <TouchableOpacity onPress={handleAddImage} style={styles.uploadButton}>
          <Icon name="plus" size={20} color="black" />
        </TouchableOpacity>
      </View>
      <FlatList
        data={Object.entries(images)}
        renderItem={({ item }) => {
          const [date, imagesForDate] = item;
          return (
            <View style={styles.dateGroup}>
              <Text style={styles.dateText}>{date}</Text>
              <View style={styles.imageGrid}>
                {imagesForDate.map((image) => (
                  <TouchableOpacity key={image._id} style={styles.imageContainer} onPress={() => handleSelectImage(image)}>
                    <Image source={{ uri: `${APIURL}/uploads/${image.imageUrl}` }} style={styles.image} />
                  </TouchableOpacity>
                ))}
              </View>
            </View>
          );
        }}
        keyExtractor={(item) => item[0]}
      />
      {/* // When rendering NavBar, pass searchQuery and setSearchQuery as props */}
      <NavBar
        searchQuery={searchQuery}
        setSearchQuery={setSearchQuery}
        currentPage={'Home'}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  dateGroup: {
    marginVertical: 8,
  },
  dateText: {
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'left',
  },
  imageGrid: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    // justifyContent: 'space-between',
    padding: 4,
  },
  imageContainer: {
    width: '32%', // Ajustez la largeur en fonction de vos besoins pour afficher trois images par ligne
    aspectRatio: 1, // Garantit que les images sont carrées
    padding: 4,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 10, // Ajustez le rayon de bordure
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // padding: 20,
    textAlign: 'center',
    backgroundColor: 'lightblue',
    height:60
  },
  title: {
    fontSize: 40,
    color: '#ffffff',
    fontFamily:'Palatino',
    // paddingLeft:130
    textAlign: 'center'

  },
  uploadButton: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    backgroundColor: 'white',
    width: 35,
    height: 35,
    margin:10,
  },
});

export default HomePage;
