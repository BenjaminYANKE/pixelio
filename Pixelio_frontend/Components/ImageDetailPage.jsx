import React, { useState } from 'react';
import { View, Text, Image, Alert, TouchableOpacity, Modal, TextInput, Button, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useNavigation } from '@react-navigation/native';

const ImageDetailPage = ({ route }) => {
  const navigation = useNavigation();
  const APIURL = 'http://10.1.5.161:3000'
  //192.168.1.16
  const { image } = route.params;
  const [isModalVisible, setModalVisible] = useState(false);
  const [albumName, setAlbumName] = useState('');
  const { removeImageFromState } = route.params;

  // Function to add an image to an album
  const addToAlbum = async (imageId, albumName) => {
    try {
      // First, check if the album exists or create a new one and get its ID
      let albumId = null;
      const responseAlbums = await fetch(`${APIURL}/albums/by-name?name=${encodeURIComponent(albumName)}`);
      const album = await responseAlbums.json();
      if (album && album._id) {
        albumId = album._id;
        await fetch(`${APIURL}/albums/add-image/${albumId}`, {
          method: 'PATCH',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ imageId: imageId }),
        });

      } else {
        const responseNewAlbum = await fetch(`${APIURL}/albums`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            albumName: albumName,
            images: [imageId]
          }),
        });
        const newAlbum = await responseNewAlbum.json();
        albumId = newAlbum._id;
      }
      // Then, associate the image with the album
      const responseImage = await fetch(`${APIURL}/images/${imageId}`, {
        method: 'PATCH',
        headers: {
          'content-Type': 'application/json',
        },
        body: JSON.stringify({ album: albumId }),
      });

      if (!responseImage.ok) {
        console.log('Failed to add the image to the album')
      }
      console.log(`Image ${imageId} added to album: ${albumName}`);
    }
    catch (error) {
      console.error('Error adding image to album:', error);
    };
  }

  // Function to add an image to favorites
  const addToFavorites = async (imageId) => {
    try {
      const response = await fetch(`${APIURL}/api/media/addToFavorites/${imageId}`, {
        method: 'POST',
      });

      if (response.ok) {

        Alert.alert("Success", "Image successfully added to Favorites!");
      } else {
        console.error('Failed to add the image to favorites');
        Alert.alert("Error", "Failed to add the image to Favorites.");
      }
    } catch (error) {
      console.error('Error adding image to favorites:', error);
      Alert.alert("Error", "An error occurred while adding the image to Favorites.");
    }
  };

  // Function to delete an image
  const deleteImage = async (imageId) => {
    try {
      const response = await fetch(`${APIURL}/images/${imageId}`, {
        method: 'DELETE',
      });
      return response;
    }
    catch (error) {
      console.error('Error deleting image:', error);
      return null;
    }
  };
  const submitAlbumName = () => {
    setModalVisible(false);
    addToAlbum(image._id, albumName);
  };

  const handleAddToAlbum = () => {
    setModalVisible(true);
  };

  const handleAddToFavorites = () => {
    addToFavorites(image._id);
  };

  const handleDeleteImage = async () => {
    Alert.alert(
      "Confirm Delete",
      "Are you sure you want to delete this image?",
      [
        {
          text: "Cancel",
          style: "cancel"
        },
        {
          text: "Delete",
          onPress: async () => {
            const response = await deleteImage(image._id); //awaits the promise
            if (response && response.ok) { //checks if response is not null before saying .ok
              removeImageFromState && removeImageFromState(image._id);
              navigation.navigate('Home')
            }
            else {
              console.error('Failed to delete the image')
            }
          },
          style: "destructive",
        }
      ]
    );
  };
  console.log(image)
  return (
    <View style={styles.container}>
      <View style={styles.header}>

        <View style={styles.actionButtons}>
          <TouchableOpacity onPress={() => handleAddToAlbum()} style={styles.addButton}>
            <Icon name="plus" size={20} color="#fff" /></TouchableOpacity>
          <TouchableOpacity onPress={() => handleAddToFavorites()}><Text>⭐</Text></TouchableOpacity>
          <TouchableOpacity onPress={() => handleDeleteImage()}><Text>🗑️</Text></TouchableOpacity>
        </View>
      </View>
      {/* Modal for adding albums */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={isModalVisible}
        onRequestClose={() => {
          setModalVisible(!isModalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <TextInput
              style={styles.textInput}
              placeholder="Enter Album Name"
              value={albumName}
              onChangeText={setAlbumName}
            />
            <Button title="Submit" onPress={submitAlbumName} />
          </View>
        </View>
      </Modal>
      <View style={styles.image}>
        <Image source={{ uri: `${APIURL}/uploads/${image.imageUrl}` }} style={{ flex: 1, resizeMode: 'contain' }} />
      </View>
      <View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    flex: 1,
    width: '100%',
    // height: '100%',
  },
  actionButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '20%',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: 20,
    backgroundColor: 'lightblue', // background color
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  textInput: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    width: 200, //another suitable width
  }
});

export default ImageDetailPage;