const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();
const app = express();
const port = 3000;

const path = require('path');
const multer = require('multer');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/');
  },
  filename: function (req, file, cb) {
    // Uses the original file extension
    const ext = path.extname(file.originalname);
    cb(null, file.fieldname + '-' + Date.now() + ext);
  }
});

const upload = multer({ storage: storage });

// Middlewares
app.use(cors());
app.use(express.json());
app.use('/uploads', express.static('uploads'));

const ensureFavoritesAlbumExists = async () => {
  const favoritesAlbum = await Album.findOne({ albumName: "Favorites" });
  if (!favoritesAlbum) {
    // "Favorites" album doesn't exist, so create it
    const newFavoritesAlbum = new Album({ albumName: "Favorites", albumDescription: "User favorite images and videos" });
    await newFavoritesAlbum.save();
    console.log('Favorites album created');

    return false;
  }

  return true;
};

// Calling this function after connecting to MongoDB
mongoose.connect(process.env.DB_CONNECTION_STRING)
  .then(() => {
    console.log('MongoDB connected...');
    ensureFavoritesAlbumExists();
  })
  .catch(err => console.log(err));

// Album Schema
const AlbumSchema = new mongoose.Schema({
  albumName: {
    type: String,
    required: true,
    trim: true
  },
  albumDescription: {
    type: String,
    trim: true
  },
  albumCreationTimestamp: {
    type: Date,
    default: Date.now
  },
  albumModificationTimestamp: {
    type: Date,
    default: Date.now
  },
  images: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Image'
  }]
});

// Image Schema
const ImageSchema = new mongoose.Schema({
  imageUrl: {
    type: String,
    required: true
  },
  imageCreationTimestamp: {
    type: Date,
    default: Date.now
  },
  album: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Album'
  }]
});

// Create Models
const Album = mongoose.model('Album', AlbumSchema);
const Image = mongoose.model('Image', ImageSchema);

// Route to get albums
app.get('/albums', async (req, res) => {
  try {
    const albums = await Album.find().lean(); // Uses .lean() for performance to avoid mongoose docs features

    for (let album of albums) {
      if (album.images.length > 0) {
        // Fetches the URL of the first image in the album
        const firstImage = await Image.findById(album.images[0]).lean();
        album.previewImageUrl = firstImage ? firstImage.imageUrl : null;
      } else {
        album.previewImageUrl = null;
      }
    }

    res.json(albums);
  } catch (error) {
    console.error('Failed to fetch albums:', error);
    res.status(500).json({ message: error.message });
  }
});

//Route to get images
app.get('/images', async (req, res) => {
  const images = await Image.find();
  res.json(images);
});


// Route for adding a new album
app.post('/albums', async (req, res) => {
  const newAlbum = new Album({
    albumName: req.body.albumName,
    images: req.body.images || []
  });

  try {
    const savedAlbum = await newAlbum.save();

    // Update each image to reference this album
    if (req.body.images && req.body.images.length > 0) {
      await Image.updateMany(
        { _id: { $in: req.body.images } },
        { $set: { album: savedAlbum._id } }
      );
    }

    res.status(201).json(savedAlbum);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Route for deleting an album
app.delete('/albums/:id', async (req, res) => {
  try {
    const album = await Album.findById(req.params.id);
    if (album.albumName === "Favorites") {
      console.log('Cette album ne peut etre supprimer');
      return res.status(403).json({ message: "Cannot delete the Favorites album." });
    }

    const deletedAlbum = await Album.findByIdAndDelete(req.params.id);
    res.status(200).json(deletedAlbum);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});


// Route for updating an album
app.patch('/albums/:id', async (req, res) => {
  try {
    const updatedAlbum = await Album.findByIdAndUpdate(
      req.params.id,
      {
        albumName: req.body.albumName
      },
      { new: true } // This option returns the modified document
    );
    res.status(200).json(updatedAlbum);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});
// Route to add an image to an existing album's images array
app.patch('/albums/add-image/:albumId', async (req, res) => {
  const { albumId } = req.params;
  const { imageId } = req.body; // The image ID to add to the album

  try {
    // Find the album and update it by adding the imageId to the images array
    const updatedAlbum = await Album.findByIdAndUpdate(
      albumId,
      { $push: { images: imageId } }, // $push to add the imageId to the images array
      { new: true } // Option to return the updated document
    );

    if (!updatedAlbum) {
      return res.status(404).json({ message: "Album not found" });
    }

    res.status(200).json(updatedAlbum);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});
// Route to get images by album ID
app.get('/albums/:albumId/images', async (req, res) => {
  try {
    const albumId = req.params.albumId;
    const album = await Album.findById(albumId).populate('images');
    if (!album) {
      return res.status(404).json({ message: 'Album not found' });
    }
    res.json(album.images);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Route for adding a new image
app.post('/images', upload.array('images', 10), async (req, res) => {
  try {
    let savedImages = [];
    for (const file of req.files) {
      const imageUrl = `${file.path.slice(8)}`; //`${req.protocol}://${req.hostname}:${port}/${file.path.replace(/\\/g, '/')}`;
      const newImage = new Image({
        imageUrl: imageUrl, // Use the image's file path
      });
      const savedImage = await newImage.save();
      savedImages.push(savedImage);
    }
    res.status(201).json(savedImages);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});


// Route for deleting an image
app.delete('/images/:id', async (req, res) => {
  try {
    const deletedImage = await Image.findByIdAndDelete(req.params.id);
    res.status(200).json(deletedImage);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});
// Route to get an album by name
app.get('/albums/by-name', async (req, res) => {
  const albumName = req.query.name;
  if (!albumName) {
    return res.status(400).json({ message: "Album name is required" });
  }

  try {
    const album = await Album.findOne({ albumName: albumName.trim() });
    if (album) {
      res.json(album);
    } else {
      res.status(404).json({ message: "Album not found" });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Route for updating an image
app.patch('/images/:id', async (req, res) => {
  try {
    const updatedImage = await Image.findByIdAndUpdate(
      req.params.id,
      {
        $set: { album: req.body.album }
      },
      { new: true }
    );
    res.status(200).json(updatedImage);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
});

// Route to add an image to the "Favorites" album
app.post('/api/media/addToFavorites/:mediaId', async (req, res) => {
  try {
    // Finding the "Favorites" album
    const favoritesAlbum = await Album.findOne({ albumName: "Favorites" });
    if (favoritesAlbum == false) {
      throw res.status(404).json({ message: "Favorite album not found." });
    }
    favoritesAlbum.images.push(req.params.mediaId);
    await favoritesAlbum.save();

    // Updates the image's album reference to "Favorites"
    const updatedImage = await Image.findByIdAndUpdate(req.params.mediaId, {
      $set: { album: favoritesAlbum._id }
    }, { new: true });

    if (updatedImage) {
      res.status(200).json(updatedImage);
    } else {
      res.status(404).json({ message: "Image not found." });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Starting the server
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
