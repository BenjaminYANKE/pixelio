# PIXELIO - Your Personal Photo Organizer

PIXELIO is a React Native mobile application designed to provide users with a seamless experience in managing and organizing their photo collections, inspired by the features of Google Photos. With PIXELIO, users can easily upload, organize, and share their photos with friends and family.

**Attention :** Pour installer correctement l'application, le serveur changeant de réseau, il est nécessaire de changer la variable *APIURL* de chaque composant en l'ip du serveur. 

## Features

1. **Photo Upload**: Users can upload photos from their phone's gallery directly to the PIXELIO app.

2. **Album Management**: Users can create, edit, and delete albums to organize their photos.

3. **Photo Grouping**: Photos are grouped by dates on the home page, providing users with a chronological view of their memories.

4. **Favorites Album**: Users can mark photos as favorites and access them quickly through the built-in Favorites album.

5. **Add to Album**: Users can add photos to specific albums for better organization.

6. **Delete Photos**: Users have the option to delete unwanted photos directly from the app.

7. **Delete Albums**: Users can delete albums, except for the Favorites album, which is a permanent feature of PIXELIO.

## Tech Stack

- Expo: Framework and platform for universal React applications.
- MongoDB: NoSQL database used to store user data, including albums and photos.
- Express.js: Backend framework for Node.js used to handle server-side logic and database connections.

## Installation

To run PIXELIO locally, follow these steps:

1. Clone this repository to your local machine.
2. Navigate to the project directory and run `npm install` to install dependencies.
3. Start the server by running `npm start` in the server directory.
4. Connect your MongoDB database and update the connection string in the server configuration file.
5. Run the Expo app using `npx expo start` and follow the instructions to launch the app on an emulator or physical device.

## Roadmap

We intend to work on advanced features for PIXELIO, including:

- **Search Functionality**: Users will be able to search for albums by name or images by months and dates.

## Support

For any questions or issues, please feel free to reach out to us at [benjamin-rachid.tchuetka-yanke@ecole-hexagone.com].

## Visuals
- MCD
![. MCD ](./Pixelio_frontend/assets/Gallerie_MCD.jpg)

- Use cases
![. Use case ](./Pixelio_frontend/assets/usecasespp.png)

- Flux Diagram
![. Diagramme de flux ](./Pixelio_frontend/assets/nouveau_flux.png)

-Maquette
![. Maquette ](./Pixelio_frontend/assets/maquette-couleur-Copie.png)

-Wireframe
![. Wireframe ](./Pixelio_frontend/assets/maquette-couleur.png)

-Gantt diagram
![. Diagramme de Gantt ](./Pixelio_frontend/assets/gantt.png)


APPLICATION PRESENTATION

-Home page
<!-- ![. le Home Page  ]() -->
<img src="./Pixelio_frontend/assets/home.jpeg" width="300" height="600" />


-Album page
<!-- ![. Page des albums ](./Pixelio_frontend/assets/album.jpeg) -->
<img src="./Pixelio_frontend/assets/album.jpeg" width="300" height="600" />


-Album content page

<!-- ![. le Album content page ](./Pixelio_frontend/assets/albumcontent.jpeg) -->
<img src="./Pixelio_frontend/assets/albumcontent.jpeg" width="300" height="600" />


-Image Details
<!-- ![. Image Detail ](./Pixelio_frontend/assets/Imagedetail.jpeg) -->
<img src="./Pixelio_frontend/assets/Imagedetail.jpeg" width="300" height="600" />




## Authors and acknowledgment
This is a group project and i'll like to acknowledge Pamela GUEKE and Thomas HOAREAU for their participations.

## License

PIXELIO is licensed under the [MIT License](LICENSE).

Thank you for using PIXELIO!
